# feca (inspired by [stay-alive](https://github.com/codealchemist/stay-alive) thanks B3rt!)

"Caffeine" substitute for OSX Mojave using 'caffeinate' (Apple's native command).
Un sustituto de caffeine para mojave usando el comando nativo de apple 'caffeinate'.

## Add aliases for using `caffeinate` on OSX

Just add them to the `.aliases` file on your `.bashrc` or `.zshrc`.

- `alias feca='/usr/bin/caffeinate -s'`
- `alias noidle='pmset noidle'`

## Installation

`npm install -g feca`

## Usage

`feca`

## caffeinate man page

```
Prevent the system from sleeping on behalf of a utility.

Syntax
     caffeinate [-disu] [-t timeout] [-w pid] [utility arguments...]

Key
  -d      Create an assertion to prevent the display from sleeping.

  -i      Create an assertion to prevent the system from idle sleeping.

  -m      Create an assertion to prevent the disk from idle sleeping.

  -s      Create an assertion to prevent the system from sleeping. This
          assertion is valid only when system is running on AC power.

  -u      Create an assertion to declare that user is active.
          If the display is off, this option turns the display on and prevents the display from going
          into idle sleep. If a timeout is not specified with '-t' option, then this assertion is
          taken with a default of 5 second timeout.

  -t      Specifies the timeout value in seconds for which this assertion has to be valid.
          The assertion is dropped after the specified timeout.
          Timeout value is not used when an utility is invoked with this command.

  -w      Waits for the process with the specified pid to exit. Once the  the process exits, the
          assertion is also released.  This option is ignored when used with utility option.

   Location: /usr/bin/caffeinate
```
