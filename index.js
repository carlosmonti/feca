#!/usr/bin/env node
const fs = require("fs");
const { spawn } = require("child_process");

console.log("::: System's awake ::: (use 'Ctrl + C' to finish the feca).");

const feca = spawn("caffeinate", ["-s"], {
  detached: true
});

feca.unref();
